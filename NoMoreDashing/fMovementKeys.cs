﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NoMoreDashing.Properties;

namespace NoMoreDashing
{
    public partial class fMovementKeys : Form {
        private fSetKeybinding frmSetForward, frmSetBackwards, frmSetLeft, frmSetRight;

        /// <summary>
        /// Initializes a new instance of the <see cref="fMovementKeys"/> class.
        /// </summary>
        public fMovementKeys() {
            InitializeComponent();
            LoadSavedSettings();
        }

        /// <summary>
        /// Loads the saved settings.
        /// </summary>
        private void LoadSavedSettings() {
            frmSetForward = new fSetKeybinding(Settings.Default.forwardKey);
            frmSetBackwards = new fSetKeybinding(Settings.Default.backwardsKey);
            frmSetLeft = new fSetKeybinding(Settings.Default.leftKey);
            frmSetRight = new fSetKeybinding(Settings.Default.rightKey);

            lblForwardKey.Text = frmSetForward.GetHotkey().ToString();
            lblLeftKey.Text = frmSetLeft.GetHotkey().ToString();
            lblBackwardsKey.Text = frmSetBackwards.GetHotkey().ToString();
            lblRightKey.Text = frmSetRight.GetHotkey().ToString();
        }

        /// <summary>
        /// Handles the Click event of the btnSetForward control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnSetForward_Click(object sender, EventArgs e) {
            frmSetForward.ShowDialog(this);
            lblForwardKey.Text = frmSetForward.GetHotkey().ToString();
        }

        /// <summary>
        /// Handles the Click event of the btnSetLeft control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnSetLeft_Click(object sender, EventArgs e) {
            frmSetLeft.ShowDialog(this);
            lblLeftKey.Text = frmSetLeft.GetHotkey().ToString();
        }

        /// <summary>
        /// Handles the Click event of the btnSetBackwards control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnSetBackwards_Click(object sender, EventArgs e) {
            frmSetBackwards.ShowDialog(this);
            lblBackwardsKey.Text = frmSetBackwards.GetHotkey().ToString();
        }

        /// <summary>
        /// Handles the Click event of the btnSetRight control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnSetRight_Click(object sender, EventArgs e) {
            frmSetRight.ShowDialog(this);
            lblRightKey.Text = frmSetRight.GetHotkey().ToString();
        }

        /// <summary>
        /// Handles the Click event of the btnSave control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnSave_Click(object sender, EventArgs e) {
            Settings.Default["forwardKey"] = frmSetForward.GetHotkey();
            Settings.Default["leftKey"] = frmSetLeft.GetHotkey();
            Settings.Default["backwardsKey"] = frmSetBackwards.GetHotkey();
            Settings.Default["rightKey"] = frmSetRight.GetHotkey();
            Settings.Default.Save();

            this.Close();
        }

        /// <summary>
        /// Handles the Click event of the btnCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnCancel_Click(object sender, EventArgs e) {
            LoadSavedSettings();
            this.Close();
        }
    }
}
