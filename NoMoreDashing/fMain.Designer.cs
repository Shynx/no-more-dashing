﻿namespace NoMoreDashing
{
    partial class fMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fMain));
            this.lblDelay = new System.Windows.Forms.Label();
            this.tbDelay = new System.Windows.Forms.TextBox();
            this.btnToggle = new System.Windows.Forms.Button();
            this.tbLog = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSetDash = new System.Windows.Forms.Button();
            this.lblCurrentDashTitle = new System.Windows.Forms.Label();
            this.lblDashKey = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mnSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.mnSettings_MovementKeys = new System.Windows.Forms.ToolStripMenuItem();
            this.mnSettings_minToTray = new System.Windows.Forms.ToolStripMenuItem();
            this.mnSettings_enableLogging = new System.Windows.Forms.ToolStripMenuItem();
            this.mnHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.mnHelp_About = new System.Windows.Forms.ToolStripMenuItem();
            this.btnUp = new System.Windows.Forms.Button();
            this.btnLeft = new System.Windows.Forms.Button();
            this.btnDown = new System.Windows.Forms.Button();
            this.btnRight = new System.Windows.Forms.Button();
            this.pnlMovementKeys = new System.Windows.Forms.Panel();
            this.menuStrip1.SuspendLayout();
            this.pnlMovementKeys.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblDelay
            // 
            this.lblDelay.AutoSize = true;
            this.lblDelay.Location = new System.Drawing.Point(12, 36);
            this.lblDelay.Name = "lblDelay";
            this.lblDelay.Size = new System.Drawing.Size(78, 13);
            this.lblDelay.TabIndex = 0;
            this.lblDelay.Text = "Minimum Delay";
            // 
            // tbDelay
            // 
            this.tbDelay.Location = new System.Drawing.Point(96, 33);
            this.tbDelay.Name = "tbDelay";
            this.tbDelay.Size = new System.Drawing.Size(73, 20);
            this.tbDelay.TabIndex = 1;
            this.tbDelay.Text = "500";
            // 
            // btnToggle
            // 
            this.btnToggle.Location = new System.Drawing.Point(15, 60);
            this.btnToggle.Name = "btnToggle";
            this.btnToggle.Size = new System.Drawing.Size(180, 26);
            this.btnToggle.TabIndex = 3;
            this.btnToggle.Text = "Start";
            this.btnToggle.UseVisualStyleBackColor = true;
            this.btnToggle.Click += new System.EventHandler(this.btnToggle_Click);
            // 
            // tbLog
            // 
            this.tbLog.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbLog.Location = new System.Drawing.Point(12, 123);
            this.tbLog.Multiline = true;
            this.tbLog.Name = "tbLog";
            this.tbLog.ReadOnly = true;
            this.tbLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbLog.Size = new System.Drawing.Size(318, 190);
            this.tbLog.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(175, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "ms";
            // 
            // btnSetDash
            // 
            this.btnSetDash.Location = new System.Drawing.Point(111, 94);
            this.btnSetDash.Name = "btnSetDash";
            this.btnSetDash.Size = new System.Drawing.Size(84, 26);
            this.btnSetDash.TabIndex = 7;
            this.btnSetDash.Text = "Set Dash Key";
            this.btnSetDash.UseVisualStyleBackColor = true;
            this.btnSetDash.Click += new System.EventHandler(this.btnSetDash_Click);
            // 
            // lblCurrentDashTitle
            // 
            this.lblCurrentDashTitle.AutoSize = true;
            this.lblCurrentDashTitle.Location = new System.Drawing.Point(12, 94);
            this.lblCurrentDashTitle.Name = "lblCurrentDashTitle";
            this.lblCurrentDashTitle.Size = new System.Drawing.Size(93, 13);
            this.lblCurrentDashTitle.TabIndex = 8;
            this.lblCurrentDashTitle.Text = "Current Dash Key:";
            // 
            // lblDashKey
            // 
            this.lblDashKey.AutoSize = true;
            this.lblDashKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDashKey.Location = new System.Drawing.Point(12, 107);
            this.lblDashKey.Name = "lblDashKey";
            this.lblDashKey.Size = new System.Drawing.Size(37, 13);
            this.lblDashKey.TabIndex = 9;
            this.lblDashKey.Text = "None";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnSettings,
            this.mnHelp});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(342, 24);
            this.menuStrip1.TabIndex = 10;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // mnSettings
            // 
            this.mnSettings.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnSettings_MovementKeys,
            this.mnSettings_minToTray,
            this.mnSettings_enableLogging});
            this.mnSettings.Name = "mnSettings";
            this.mnSettings.Size = new System.Drawing.Size(61, 20);
            this.mnSettings.Text = "Settings";
            // 
            // mnSettings_MovementKeys
            // 
            this.mnSettings_MovementKeys.Name = "mnSettings_MovementKeys";
            this.mnSettings_MovementKeys.Size = new System.Drawing.Size(162, 22);
            this.mnSettings_MovementKeys.Text = "Movement Keys";
            this.mnSettings_MovementKeys.Click += new System.EventHandler(this.mnSettings_MovementKeys_Click);
            // 
            // mnSettings_minToTray
            // 
            this.mnSettings_minToTray.CheckOnClick = true;
            this.mnSettings_minToTray.Name = "mnSettings_minToTray";
            this.mnSettings_minToTray.Size = new System.Drawing.Size(162, 22);
            this.mnSettings_minToTray.Text = "Minimize to Tray";
            this.mnSettings_minToTray.Click += new System.EventHandler(this.mnSettings_minToTray_Click);
            // 
            // mnSettings_enableLogging
            // 
            this.mnSettings_enableLogging.CheckOnClick = true;
            this.mnSettings_enableLogging.Name = "mnSettings_enableLogging";
            this.mnSettings_enableLogging.Size = new System.Drawing.Size(162, 22);
            this.mnSettings_enableLogging.Text = "Enable logging";
            this.mnSettings_enableLogging.Click += new System.EventHandler(this.mnSettings_enableLogging_Click);
            // 
            // mnHelp
            // 
            this.mnHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnHelp_About});
            this.mnHelp.Name = "mnHelp";
            this.mnHelp.Size = new System.Drawing.Size(44, 20);
            this.mnHelp.Text = "Help";
            // 
            // mnHelp_About
            // 
            this.mnHelp_About.Name = "mnHelp_About";
            this.mnHelp_About.Size = new System.Drawing.Size(107, 22);
            this.mnHelp_About.Text = "About";
            this.mnHelp_About.Click += new System.EventHandler(this.mnHelp_About_Click);
            // 
            // btnUp
            // 
            this.btnUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUp.Location = new System.Drawing.Point(47, 4);
            this.btnUp.Name = "btnUp";
            this.btnUp.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnUp.Size = new System.Drawing.Size(35, 35);
            this.btnUp.TabIndex = 11;
            this.btnUp.Text = "↑";
            this.btnUp.UseVisualStyleBackColor = true;
            // 
            // btnLeft
            // 
            this.btnLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLeft.Location = new System.Drawing.Point(6, 45);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(35, 35);
            this.btnLeft.TabIndex = 12;
            this.btnLeft.Text = "←";
            this.btnLeft.UseVisualStyleBackColor = true;
            // 
            // btnDown
            // 
            this.btnDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDown.Location = new System.Drawing.Point(47, 45);
            this.btnDown.Name = "btnDown";
            this.btnDown.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnDown.Size = new System.Drawing.Size(35, 35);
            this.btnDown.TabIndex = 13;
            this.btnDown.Text = "↓";
            this.btnDown.UseVisualStyleBackColor = true;
            // 
            // btnRight
            // 
            this.btnRight.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.btnRight.Location = new System.Drawing.Point(88, 45);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(35, 35);
            this.btnRight.TabIndex = 14;
            this.btnRight.Text = "→";
            this.btnRight.UseVisualStyleBackColor = false;
            // 
            // pnlMovementKeys
            // 
            this.pnlMovementKeys.Controls.Add(this.btnLeft);
            this.pnlMovementKeys.Controls.Add(this.btnRight);
            this.pnlMovementKeys.Controls.Add(this.btnUp);
            this.pnlMovementKeys.Controls.Add(this.btnDown);
            this.pnlMovementKeys.Location = new System.Drawing.Point(201, 34);
            this.pnlMovementKeys.Name = "pnlMovementKeys";
            this.pnlMovementKeys.Size = new System.Drawing.Size(129, 83);
            this.pnlMovementKeys.TabIndex = 15;
            // 
            // fMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(342, 325);
            this.Controls.Add(this.pnlMovementKeys);
            this.Controls.Add(this.lblDashKey);
            this.Controls.Add(this.lblCurrentDashTitle);
            this.Controls.Add(this.btnSetDash);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbLog);
            this.Controls.Add(this.btnToggle);
            this.Controls.Add(this.tbDelay);
            this.Controls.Add(this.lblDelay);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "fMain";
            this.Text = "NMD";
            this.Load += new System.EventHandler(this.frmMainLoad);
            this.Resize += new System.EventHandler(this.frmMain_Resize);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.pnlMovementKeys.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDelay;
        private System.Windows.Forms.TextBox tbDelay;
        private System.Windows.Forms.Button btnToggle;
        private System.Windows.Forms.TextBox tbLog;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSetDash;
        private System.Windows.Forms.Label lblCurrentDashTitle;
        private System.Windows.Forms.Label lblDashKey;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mnHelp;
        private System.Windows.Forms.ToolStripMenuItem mnHelp_About;
        private System.Windows.Forms.ToolStripMenuItem mnSettings;
        private System.Windows.Forms.ToolStripMenuItem mnSettings_MovementKeys;
        private System.Windows.Forms.ToolStripMenuItem mnSettings_minToTray;
        private System.Windows.Forms.ToolStripMenuItem mnSettings_enableLogging;
        private System.Windows.Forms.Button btnUp;
        private System.Windows.Forms.Button btnLeft;
        private System.Windows.Forms.Button btnDown;
        private System.Windows.Forms.Button btnRight;
        private System.Windows.Forms.Panel pnlMovementKeys;
    }
}

