﻿namespace NoMoreDashing
{
    partial class fMovementKeys
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblForward = new System.Windows.Forms.Label();
            this.lblLeft = new System.Windows.Forms.Label();
            this.lblBackwards = new System.Windows.Forms.Label();
            this.lblRight = new System.Windows.Forms.Label();
            this.btnSetForward = new System.Windows.Forms.Button();
            this.btnSetLeft = new System.Windows.Forms.Button();
            this.btnSetBackwards = new System.Windows.Forms.Button();
            this.btnSetRight = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblRightKey = new System.Windows.Forms.Label();
            this.lblBackwardsKey = new System.Windows.Forms.Label();
            this.lblLeftKey = new System.Windows.Forms.Label();
            this.lblForwardKey = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblForward
            // 
            this.lblForward.AutoSize = true;
            this.lblForward.Location = new System.Drawing.Point(13, 18);
            this.lblForward.Name = "lblForward";
            this.lblForward.Size = new System.Drawing.Size(48, 13);
            this.lblForward.TabIndex = 0;
            this.lblForward.Text = "Forward:";
            // 
            // lblLeft
            // 
            this.lblLeft.AutoSize = true;
            this.lblLeft.Location = new System.Drawing.Point(13, 47);
            this.lblLeft.Name = "lblLeft";
            this.lblLeft.Size = new System.Drawing.Size(59, 13);
            this.lblLeft.TabIndex = 1;
            this.lblLeft.Text = "Strafe Left:";
            // 
            // lblBackwards
            // 
            this.lblBackwards.AutoSize = true;
            this.lblBackwards.Location = new System.Drawing.Point(13, 77);
            this.lblBackwards.Name = "lblBackwards";
            this.lblBackwards.Size = new System.Drawing.Size(63, 13);
            this.lblBackwards.TabIndex = 2;
            this.lblBackwards.Text = "Backwards:";
            // 
            // lblRight
            // 
            this.lblRight.AutoSize = true;
            this.lblRight.Location = new System.Drawing.Point(13, 107);
            this.lblRight.Name = "lblRight";
            this.lblRight.Size = new System.Drawing.Size(66, 13);
            this.lblRight.TabIndex = 3;
            this.lblRight.Text = "Strafe Right:";
            // 
            // btnSetForward
            // 
            this.btnSetForward.Location = new System.Drawing.Point(127, 12);
            this.btnSetForward.Name = "btnSetForward";
            this.btnSetForward.Size = new System.Drawing.Size(75, 23);
            this.btnSetForward.TabIndex = 4;
            this.btnSetForward.Text = "Set";
            this.btnSetForward.UseVisualStyleBackColor = true;
            this.btnSetForward.Click += new System.EventHandler(this.btnSetForward_Click);
            // 
            // btnSetLeft
            // 
            this.btnSetLeft.Location = new System.Drawing.Point(127, 41);
            this.btnSetLeft.Name = "btnSetLeft";
            this.btnSetLeft.Size = new System.Drawing.Size(75, 23);
            this.btnSetLeft.TabIndex = 5;
            this.btnSetLeft.Text = "Set";
            this.btnSetLeft.UseVisualStyleBackColor = true;
            this.btnSetLeft.Click += new System.EventHandler(this.btnSetLeft_Click);
            // 
            // btnSetBackwards
            // 
            this.btnSetBackwards.Location = new System.Drawing.Point(127, 71);
            this.btnSetBackwards.Name = "btnSetBackwards";
            this.btnSetBackwards.Size = new System.Drawing.Size(75, 23);
            this.btnSetBackwards.TabIndex = 6;
            this.btnSetBackwards.Text = "Set";
            this.btnSetBackwards.UseVisualStyleBackColor = true;
            this.btnSetBackwards.Click += new System.EventHandler(this.btnSetBackwards_Click);
            // 
            // btnSetRight
            // 
            this.btnSetRight.Location = new System.Drawing.Point(127, 101);
            this.btnSetRight.Name = "btnSetRight";
            this.btnSetRight.Size = new System.Drawing.Size(75, 23);
            this.btnSetRight.TabIndex = 7;
            this.btnSetRight.Text = "Set";
            this.btnSetRight.UseVisualStyleBackColor = true;
            this.btnSetRight.Click += new System.EventHandler(this.btnSetRight_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Location = new System.Drawing.Point(12, 149);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(128, 149);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblRightKey
            // 
            this.lblRightKey.AutoSize = true;
            this.lblRightKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRightKey.Location = new System.Drawing.Point(85, 107);
            this.lblRightKey.Name = "lblRightKey";
            this.lblRightKey.Size = new System.Drawing.Size(16, 13);
            this.lblRightKey.TabIndex = 10;
            this.lblRightKey.Text = "D";
            // 
            // lblBackwardsKey
            // 
            this.lblBackwardsKey.AutoSize = true;
            this.lblBackwardsKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBackwardsKey.Location = new System.Drawing.Point(85, 77);
            this.lblBackwardsKey.Name = "lblBackwardsKey";
            this.lblBackwardsKey.Size = new System.Drawing.Size(15, 13);
            this.lblBackwardsKey.TabIndex = 11;
            this.lblBackwardsKey.Text = "S";
            // 
            // lblLeftKey
            // 
            this.lblLeftKey.AutoSize = true;
            this.lblLeftKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLeftKey.Location = new System.Drawing.Point(85, 47);
            this.lblLeftKey.Name = "lblLeftKey";
            this.lblLeftKey.Size = new System.Drawing.Size(15, 13);
            this.lblLeftKey.TabIndex = 12;
            this.lblLeftKey.Text = "A";
            // 
            // lblForwardKey
            // 
            this.lblForwardKey.AutoSize = true;
            this.lblForwardKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblForwardKey.Location = new System.Drawing.Point(85, 18);
            this.lblForwardKey.Name = "lblForwardKey";
            this.lblForwardKey.Size = new System.Drawing.Size(19, 13);
            this.lblForwardKey.TabIndex = 13;
            this.lblForwardKey.Text = "W";
            // 
            // MovementKeys
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(215, 180);
            this.Controls.Add(this.lblForwardKey);
            this.Controls.Add(this.lblLeftKey);
            this.Controls.Add(this.lblBackwardsKey);
            this.Controls.Add(this.lblRightKey);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnSetRight);
            this.Controls.Add(this.btnSetBackwards);
            this.Controls.Add(this.btnSetLeft);
            this.Controls.Add(this.btnSetForward);
            this.Controls.Add(this.lblRight);
            this.Controls.Add(this.lblBackwards);
            this.Controls.Add(this.lblLeft);
            this.Controls.Add(this.lblForward);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MovementKeys";
            this.Text = "MovementKeys";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblForward;
        private System.Windows.Forms.Label lblLeft;
        private System.Windows.Forms.Label lblBackwards;
        private System.Windows.Forms.Label lblRight;
        private System.Windows.Forms.Button btnSetForward;
        private System.Windows.Forms.Button btnSetLeft;
        private System.Windows.Forms.Button btnSetBackwards;
        private System.Windows.Forms.Button btnSetRight;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblRightKey;
        private System.Windows.Forms.Label lblBackwardsKey;
        private System.Windows.Forms.Label lblLeftKey;
        private System.Windows.Forms.Label lblForwardKey;
    }
}