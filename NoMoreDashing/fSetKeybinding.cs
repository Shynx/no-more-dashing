﻿using System;
using System.Windows.Forms;
using Gma.UserActivityMonitor;

namespace NoMoreDashing
{
    using NoMoreDashing.Properties;

    public partial class fSetKeybinding : Form {
        private Keys currentHotkey, setHotkey;
        /// <summary>
        /// Initializes a new instance of the <see cref="fSetKeybinding"/> class.
        /// </summary>
        public fSetKeybinding() {
            InitializeComponent();
        }

        public fSetKeybinding(Keys k) {
            InitializeComponent();
            setHotkey = k;
        }

        /// <summary>
        /// Handles the Click event of the btnAccept control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnAccept_Click(object sender, EventArgs e) {
            setHotkey = currentHotkey;
            this.Close();
        }

        /// <summary>
        /// Handles the Click event of the btnReset control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnReset_Click(object sender, EventArgs e) {
            setHotkey = Keys.None;
            this.Close();
        }

        /// <summary>
        /// Handles the Load event of the frmSetDash control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void frmSetDash_Load(object sender, EventArgs e) {

        }

        /// <summary>
        /// Sets the current key.
        /// </summary>
        /// <param name="k">The k.</param>
        public void SetHotkey(Keys k) {
            setHotkey = k;
        }

        /// <summary>
        /// Gets the current key.
        /// </summary>
        /// <returns></returns>
        public Keys GetHotkey() {
            return setHotkey;
        }

        /// <summary>
        /// Handles the Shown event of the frmSetDash control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void frmSetDash_Shown(object sender, EventArgs e) {
            HookManager.KeyUp += HookManager_KeyUp;
            lblInput.Text = Resources.frmSetDash_Shown_Waiting_for_input;
        }

        /// <summary>
        /// Handles the KeyPress event of the HookManager control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyPressEventArgs"/> instance containing the event data.</param>
        private void HookManager_KeyUp(object sender, KeyEventArgs e) {
            currentHotkey = e.KeyCode;
            lblInput.Text = @"New Hotkey: " + e.KeyCode;
        }

        private void frmSetDash_FormClosing(object sender, FormClosingEventArgs e) {
            HookManager.KeyUp -= HookManager_KeyUp;
        }

    }
}
