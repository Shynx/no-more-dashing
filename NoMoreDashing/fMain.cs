﻿using System;
using System.Windows.Forms;
using Gma.UserActivityMonitor;
using WindowsInput;
using System.Drawing;
using WindowsInput.Native;
using NoMoreDashing.Properties;

namespace NoMoreDashing
{
    public partial class fMain : Form {
        private readonly InputSimulator ip = new InputSimulator();
        private fSetKeybinding setDash;
        private fAbout frmAbout;
        private fMovementKeys frmMovementKeys;
        private bool active;
        private KeyManager keyManager;

        private NotifyIcon trayIcon;
        private ContextMenu trayContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="fMain"/> class.
        /// </summary>
        public fMain() {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the Load event of the form.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        void frmMainLoad(object sender, EventArgs e) {
            InitTrayIcon();
            setDash = new fSetKeybinding { ShowInTaskbar = false };
            frmAbout = new fAbout();
            frmMovementKeys = new fMovementKeys();
            keyManager = new KeyManager(btnUp, btnLeft, btnRight, btnDown);

            //load settings
            tbDelay.Text = Settings.Default.delay.ToString();
            mnSettings_minToTray.Checked = Settings.Default.minToTray;
            mnSettings_enableLogging.Checked = Settings.Default.enableLog;
            if (mnSettings_enableLogging.Checked) {
                this.Height = 364;
                tbLog.Visible = true;
            } else {
                this.Height = 164;
                tbLog.Visible = false;
            }
            setDash.SetHotkey(Settings.Default.dashKey);
            lblDashKey.Text = Settings.Default.dashKey.ToString();
        }

        /// <summary>
        /// Handles the Resize event of the frmMain control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void frmMain_Resize(object sender, EventArgs e) {
            if (!Settings.Default.minToTray) return;
            switch (this.WindowState) {
                case FormWindowState.Minimized:
                    this.trayIcon.Visible = true;
                    this.Hide();
                    break;
                case FormWindowState.Normal:
                    this.Show();
                    this.Focus();
                    this.trayIcon.Visible = false;
                    break;
            }
        }

        /// <summary>
        /// Initializes the tray icon.
        /// </summary>
        private void InitTrayIcon() {
            trayContext = new ContextMenu();
            trayContext.MenuItems.Add("Start", OnStart);
            trayContext.MenuItems.Add("Stop", OnStop);
            trayContext.MenuItems.Add("-");
            trayContext.MenuItems.Add("Show", OnShow);
            trayContext.MenuItems.Add("Exit", OnExit);

            trayIcon = new NotifyIcon {
                Text = Resources.appTitle,
                Icon = new Icon(Resources.nmd_icon, 40, 40),
                ContextMenu = trayContext,
                Visible = false
            };
            trayIcon.DoubleClick += OnShow;
        }
        
        /// <summary>
        /// Called when [start].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnStart(object sender, EventArgs e) {
            this.Start();
        }

        /// <summary>
        /// Called when [stop].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnStop(object sender, EventArgs e) {
            this.Stop();
        }

        /// <summary>
        /// Called when [show].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnShow(object sender, EventArgs e) {
            this.Show();
            this.Focus();
            this.trayIcon.Visible = false;
            this.WindowState = FormWindowState.Normal;
        }

        /// <summary>
        /// Called when [exit].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnExit(object sender, EventArgs e) {
            this.Stop();
            Application.Exit();
        }

        /// <summary>
        /// Handles the Click event of the btnToggle control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnToggle_Click(object sender, EventArgs e) {
            if (!active) {
                this.Start();
            } else {
                this.Stop();
            }
        }
        
        /// <summary>
        /// Handles the CheckedChanged event of the cbLog control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        /// <exception cref="System.Exception">An Error occured</exception>
        private void mnSettings_enableLogging_Click(object sender, EventArgs e) {
            var s = (ToolStripMenuItem)sender;
            if(s == null) throw new Exception("An Error occured");
            if (s.Checked) {
                tbLog.Visible = true;
                this.Height = 364;
                Settings.Default["enableLog"] = true;
            } else {
                tbLog.Visible = false;
                this.Height = 164;
                Settings.Default["enableLog"] = false;
            }
            Settings.Default.Save();
        }

        /// <summary>
        /// Handles the Click event of the mnSettings_MovementKeys control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void mnSettings_MovementKeys_Click(object sender, EventArgs e) {
            frmMovementKeys.ShowDialog(this);
        }

        /// <summary>
        /// Handles the CheckedChanged event of the cbMinToTray control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void mnSettings_minToTray_Click(object sender, EventArgs e) {
            var s = (ToolStripMenuItem)sender;
            if(s == null) throw new Exception("An Error occured");
            if (s.Checked) {
                Settings.Default["minToTray"] = true;
            } else {
                Settings.Default["minToTray"] = false;
            }
            Settings.Default.Save();
        }

        /// <summary>
        /// Handles the Click event of the btnSetDash control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnSetDash_Click(object sender, EventArgs e) {
            setDash.ShowDialog(this);
            lblDashKey.Text = setDash.GetHotkey().ToString();
            Settings.Default["dashKey"] = setDash.GetHotkey();
            Settings.Default.Save();
        }

        /// <summary>
        /// Handles the Click event of the mnHelp_About control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void mnHelp_About_Click(object sender, EventArgs e) {
            frmAbout.ShowDialog(this);
        }

        /// <summary>
        /// Handles the KeyDown event of the HookManager.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
        private void HookManager_KeyDown(object sender, KeyEventArgs e) {
            if (!active) return;
            if (e.KeyCode == Settings.Default.dashKey) {
                e.SuppressKeyPress = true;
                ip.Keyboard.KeyPress(VirtualKeyCode.LSHIFT);
                this.Log("Dash Key pressed, sending LShift");
                return;
            }
            if (keyManager.Keypress(e)) this.Log(e.KeyCode + " pressed too fast. Suppressing keystroke.");
        }
        
        /// <summary>
        /// Handles the KeyUp event of the HookManager.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
        private void HookManager_KeyUp(object sender, KeyEventArgs e) {
            if (!this.active) return;
            if (keyManager.Keyrelease(e)) Log(e.KeyCode + " released. Starting timer");
        }
        
        /// <summary>
        /// Starts Key Delay.
        /// </summary>
        private void Start() {
            active = true;
            tbDelay.ReadOnly = true;
            int delay;
            if (!int.TryParse(tbDelay.Text, out delay) || tbDelay.Text == string.Empty) {
                Log("Input could not be converted to number. Using default delay of 1000ms.");
                delay = 500;
                tbDelay.Text = @"500";
            }
            Settings.Default["delay"] = delay;
            Settings.Default.Save();
            HookManager.KeyDown += HookManager_KeyDown;
            HookManager.KeyUp += HookManager_KeyUp;
            this.btnToggle.Text = Resources.frmMain_btnToggle_Click_Stop;
            trayContext.MenuItems[0].Enabled = false;
            trayContext.MenuItems[1].Enabled = true;
        }

        /// <summary>
        /// Stops Key Delay.
        /// </summary>
        private void Stop() {
            active = false;
            tbDelay.ReadOnly = false;
            HookManager.KeyDown -= HookManager_KeyDown;
            HookManager.KeyUp -= HookManager_KeyUp;
            this.btnToggle.Text = Resources.frmMain_btnToggle_Click_Start;
            trayContext.MenuItems[0].Enabled = true;
            trayContext.MenuItems[1].Enabled = false;
        }
        
        /// <summary>
        /// Logs the specified text.
        /// </summary>
        /// <param name="text">The text.</param>
        private void Log(string text) {
            if (!Settings.Default.enableLog) return;
            tbLog.AppendText(DateTime.Now.ToString("HH:mm:ss.fff") + " - " + text + "\r\n");
            tbLog.ScrollToCaret();
        }
    }
}
