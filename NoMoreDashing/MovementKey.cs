﻿using System;
using System.Windows.Forms;
using System.Diagnostics;
using System.Drawing;
using WindowsInput;
namespace NoMoreDashing
{
    using System.Runtime.Remoting.Messaging;
    using System.Windows.Input;

    using Properties;

    public enum Direction {
        Forward,
        Backwards,
        Left,
        Right
    }

    public class MovementKey {
        public Direction direction;
        private bool isPressed;
        public long lastSent;
        private readonly Keys key;
        public Control linkedControl;
        private Color defaultBackground, defaultForeground;
        private readonly Stopwatch sw;

        public MovementKey(Direction dir, Keys k, Control ctrl) {
            direction = dir;
            key = k;
            linkedControl = ctrl;
            defaultBackground = SystemColors.ControlLight;
            defaultForeground = linkedControl.ForeColor;
            sw = new Stopwatch();
            sw.Start();
        }

        public bool Press() {
            isPressed = true;            
            lastSent = DateTime.Now.Ticks;
            linkedControl.BackColor = Color.Crimson;
            linkedControl.ForeColor = Color.White;
            if (this.sw.ElapsedMilliseconds > Settings.Default.delay) return false;
            return true;
        }

        public void Release() {
            isPressed = false;
            linkedControl.BackColor = defaultBackground;
            linkedControl.ForeColor = defaultForeground;
            if(sw.IsRunning && this.sw.ElapsedMilliseconds > Settings.Default.delay) sw.Restart();
        }

        public void Refresh() {
            if(isPressed && Keyboard.IsKeyUp((Key)key)) this.Release();
            if(!isPressed && Keyboard.IsKeyDown((Key)key)) this.Press();
        }

        public bool IsPressed() {
            return isPressed;
        }

        public bool IsKey(Keys k) {
            return k == this.key;
        }

        public Keys GetKey() {
            return key;
        }

        public string GetKeyString() {
            return key.ToString();
        }
    }
}
