﻿using System.Windows.Forms;
using System.Windows.Input;
using System.Collections.Generic;

namespace NoMoreDashing
{
    using Properties;
    using KeyEventArgs = System.Windows.Forms.KeyEventArgs;

    class KeyManager
    {
        private readonly MovementKey forwardKey, leftKey, rightKey, backwardsKey;
        private readonly List<MovementKey> pressedKeys = new List<MovementKey>();

        public KeyManager(Control forwardControl, Control leftControl, Control rightControl, Control backwardsControl) {
            forwardKey = new MovementKey(Direction.Forward, Settings.Default.forwardKey, forwardControl);
            leftKey = new MovementKey(Direction.Left, Settings.Default.leftKey, leftControl);
            rightKey = new MovementKey(Direction.Right, Settings.Default.rightKey, rightControl);
            backwardsKey = new MovementKey(Direction.Backwards, Settings.Default.backwardsKey, backwardsControl);
        }

        public bool Keypress(KeyEventArgs e) {
            if (forwardKey.IsKey(e.KeyCode)) {
                if (forwardKey.IsPressed()) return false;
                e.SuppressKeyPress = forwardKey.Press();
                MarkAsPressed(forwardKey);
            } else if (leftKey.IsKey(e.KeyCode)) {
                //if(Keyboard.IsKeyDown(Key.A)) return false;
                //if (leftKey.IsPressed()) return false;
                e.SuppressKeyPress = leftKey.Press();
                MarkAsPressed(leftKey);
            } else if (backwardsKey.IsKey(e.KeyCode)) {
                if (backwardsKey.IsPressed()) return false;
                e.SuppressKeyPress = backwardsKey.Press();
                MarkAsPressed(backwardsKey);
            } else if (rightKey.IsKey(e.KeyCode)) {
                if (rightKey.IsPressed()) return false;
                e.SuppressKeyPress = rightKey.Press();
                MarkAsPressed(rightKey);
            } else return false;

            //if more than 3 buttons are pressed, release the one first pressed
            if (pressedKeys.Count > 3) {
                pressedKeys[0].Release();
                pressedKeys.RemoveAt(0);
            }

            return e.SuppressKeyPress;
        }

        public bool Keyrelease(KeyEventArgs e) {
            if (forwardKey.IsKey(e.KeyCode)) {
                forwardKey.Release();
                MarkAsReleased(forwardKey);
            } else if (leftKey.IsKey(e.KeyCode)) {
                leftKey.Release();
                MarkAsReleased(leftKey);
            } else if (backwardsKey.IsKey(e.KeyCode)) {
                backwardsKey.Release();
                MarkAsReleased(backwardsKey);
            } else if (rightKey.IsKey(e.KeyCode)) {
                rightKey.Release();
                MarkAsReleased(rightKey);
            } else return false;

            return true;
        }

        private void MarkAsPressed(MovementKey mk) {
            for (int i = 0; i < pressedKeys.Count; i++) {
                if(pressedKeys[i] == mk) pressedKeys.RemoveAt(i);
            }
            pressedKeys.Add(mk);
        }

        private void MarkAsReleased(MovementKey mk) {
            if (!pressedKeys.Contains(mk)) return;
            for (int i = 0; i < pressedKeys.Count; i++) {
                if(pressedKeys[i] == mk) pressedKeys.RemoveAt(i);
            }
        }

        public void RefreshKeystates() {
            forwardKey.Refresh();
            leftKey.Refresh();
            backwardsKey.Refresh();
            rightKey.Refresh();
        }
    }
}
